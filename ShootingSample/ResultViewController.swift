//
//  ResultViewController.swift
//  ShootingSample
//
//  Created by Nobuhiro on 2015/01/11.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import UIKit
import GameKit

class ResultViewController: UIViewController, GADInterstitialDelegate, GKGameCenterControllerDelegate, NADIconLoaderDelegate, CommonAlertDelegate {
    // MARK: OUTLET
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var maxScoreLabel: UILabel!

    @IBOutlet weak var nadIconArrayView: NADIconArrayView!
    
    var interstitial: GADInterstitial = GADInterstitial()
    var interstitialRequest = GADRequest()
    var bannerViewFooter: GADBannerView?
    let request = GADRequest()
    
    var score = 0
    var maxScore = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = baseColor

        // フッター広告
        var offsetX = (self.view.frame.size.width - kGADAdSizeBanner.size.width) / 2
        bannerViewFooter = GADBannerView(adSize: kGADAdSizeBanner, origin: CGPointMake(offsetX, self.view.frame.size.height - kGADAdSizeBanner.size.height))
        bannerViewFooter?.adUnitID = "ca-app-pub-8756306138420194/4903153661" // 広告ユニットID
        bannerViewFooter?.rootViewController = self
        self.view.addSubview(bannerViewFooter!)
        bannerViewFooter?.loadRequest(request)
        
        // インタースティシャル広告表示用
        self.interstitial.delegate = self
        self.interstitial.adUnitID = "ca-app-pub-8756306138420194/6379886865"
        //self.interstitialRequest.testDevices = ["0c564c011ea6e55b1c06ec5b17863b8a", GAD_SIMULATOR_ID]
        self.interstitial.loadRequest(self.interstitialRequest)
        
        // IconAdd
        self.nadIconArrayView.textHidden = true
        
        self.maxScore = NSUserDefaults.standardUserDefaults().integerForKey("max_score")
        if (score > maxScore) {
            // new record
            NSUserDefaults.standardUserDefaults().setInteger(score, forKey: "max_score")
            maxScore = score
            self.reportScoreToGameCenter(score)
        }
        self.scoreLabel.text = String(score)
        self.maxScoreLabel.text = String(maxScore)
        self.message.text = getResultMessage(score)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.nadIconArrayView.iconLoader.resume()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.nadIconArrayView.iconLoader.pause()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func moveToTopButtonPushed(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    @IBAction func retryButtonPushed(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // notice : this is "ranking" button
    @IBAction func moveToMenuButtonPushed(sender: AnyObject) {
        var localPlayer = GKLocalPlayer()
        
        localPlayer.loadDefaultLeaderboardIdentifierWithCompletionHandler({ (leaderboardIdentifier : String!, error : NSError!) -> Void in
            if error != nil {
                CommonAlert(delegate: self).showAlert(self, message: "ランキング機能を使用するにはGameCenterにサインインしてください")
                println(error.localizedDescription)
            } else {
                let gameCenterController:GKGameCenterViewController = GKGameCenterViewController()
                gameCenterController.gameCenterDelegate = self
                gameCenterController.viewState = GKGameCenterViewControllerState.Leaderboards
                gameCenterController.leaderboardIdentifier = "Eiden01" //該当するLeaderboardのIDを指定します
                self.presentViewController(gameCenterController, animated: true, completion: nil)
            }
        })
    }
    
    func interstitialDidReceiveAd(ad: GADInterstitial!) {
        println("interstitial Ad received")
        // インタースティシャル広告の表示判定
        resultInsAddDispCounter++
        if (resultInsAddDispCounter >= INTER_SITIAL_INTERVAL) {
            resultInsAddDispCounter = 0
            self.interstitial.presentFromRootViewController(self)
        }
    }
    
    private func reportScoreToGameCenter(value:Int){
        var score:GKScore = GKScore()
        score.value = Int64(value)
        score.leaderboardIdentifier = "Eiden01"
        var scoreArr:[GKScore] = [score]
        GKScore.reportScores(scoreArr, withCompletionHandler:{(error:NSError!) -> Void in
            if( (error != nil)){
                println("reportScore NG \n\(score)")
            }else{
                println("reportScore OK \n\(score)")
            }
        })
    }
    
    // Delegate method for GKGameCenterDelegate
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController!){
        gameCenterViewController.dismissViewControllerAnimated(true, completion: nil);
    }
    
    // CommonAlertDelegate
    func buttonTapped(tag: Int) {
        switch tag {
        case 0 :
            self.dismissViewControllerAnimated(true, completion: nil)
            break
        default:
            break
        }
    }
    
    // UIAlertViewDelegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        switch buttonIndex {
        case 0 :
            switch alertView.tag {
            case 0 :
                self.dismissViewControllerAnimated(true, completion: nil)
                break
            case 1 :
                break
            default :
                break
            }
            break
        default:
            break
        }
    }
    
    deinit {
        self.nadIconArrayView.iconLoader.delegate = nil
    }
}
