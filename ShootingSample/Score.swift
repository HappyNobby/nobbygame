//
//  Score.swift
//  ShootingSample
//
//  Created by Nobuhiro on 2015/01/12.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import UIKit
import SpriteKit

class Score: SKSpriteNode {
    let DIGIT = 7
    let MAX_VALUE = 9999999
    let NUMBER_SIZE = CGSize(width: 15, height: 20)
    let TOTAL_SIZE = CGSize(width: 105, height: 20)
    
    var digitTexture01: SKSpriteNode = SKSpriteNode(texture: numberTexture00)
    var digitTexture02: SKSpriteNode = SKSpriteNode(texture: numberTexture00)
    var digitTexture03: SKSpriteNode = SKSpriteNode(texture: numberTexture00)
    var digitTexture04: SKSpriteNode = SKSpriteNode(texture: numberTexture00)
    var digitTexture05: SKSpriteNode = SKSpriteNode(texture: numberTexture00)
    var digitTexture06: SKSpriteNode = SKSpriteNode(texture: numberTexture00)
    var digitTexture07: SKSpriteNode = SKSpriteNode(texture: numberTexture00)
    
    var defaultScale: CGFloat = 1.0
    var updateScale: CGFloat = 1.0
    
    
    private var value = 0
    
    override init() {
        super.init()
        
        digitTexture01.size = NUMBER_SIZE
        digitTexture02.size = NUMBER_SIZE
        digitTexture03.size = NUMBER_SIZE
        digitTexture04.size = NUMBER_SIZE
        digitTexture05.size = NUMBER_SIZE
        digitTexture06.size = NUMBER_SIZE
        digitTexture07.size = NUMBER_SIZE
        self.defaultScale = self.digitTexture01.xScale
        self.updateScale = self.defaultScale * 1.5
        
        
        digitTexture01.position = CGPoint(x: NUMBER_SIZE.width * 6, y: 0)
        digitTexture02.position = CGPoint(x: NUMBER_SIZE.width * 5, y: 0)
        digitTexture03.position = CGPoint(x: NUMBER_SIZE.width * 4, y: 0)
        digitTexture04.position = CGPoint(x: NUMBER_SIZE.width * 3, y: 0)
        digitTexture05.position = CGPoint(x: NUMBER_SIZE.width * 2, y: 0)
        digitTexture06.position = CGPoint(x: NUMBER_SIZE.width * 1, y: 0)
        digitTexture07.position = CGPoint(x: 0, y: 0)
        
        addChild(digitTexture01)
        addChild(digitTexture02)
        addChild(digitTexture03)
        addChild(digitTexture04)
        addChild(digitTexture05)
        addChild(digitTexture06)
        addChild(digitTexture07)
    }
    override init(texture: SKTexture!, color: UIColor!, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setScore(score: Int) {
        if (score > self.MAX_VALUE) {
            self.value = self.MAX_VALUE
        } else if (score < 0){
            self.value = 0
        } else {
            self.value = score
        }
        
        self.setNumberTexture(self.value)
    }
    
    func getScore() -> Int {
        return self.value
    }
    
    func addScore(addValue: Int) {
        var newValue = self.value + addValue
        self.setScore(newValue)
    }
    
    private func setNumberTexture(number: Int) {
        if (number > self.MAX_VALUE || number < 0) {
            return
        }
        // 各桁の数値を取得
        var tmp = self.value
        var digit07: Int = self.value / 1000000
        tmp -= digit07 * 1000000
        
        var digit06: Int = tmp / 100000
        tmp -= digit06 * 100000
        
        var digit05: Int = tmp / 10000
        tmp -= digit05 * 10000
        
        var digit04: Int = tmp / 1000
        tmp -= digit04 * 1000
        
        var digit03: Int = tmp / 100
        tmp -= digit03 * 100
        
        var digit02: Int = tmp / 10
        tmp -= digit02 * 10
        
        var digit01: Int = tmp
        
        // texture set
        self.digitTexture01.texture = getNumberTexture(digit01)
        self.digitTexture02.texture = getNumberTexture(digit02)
        self.digitTexture03.texture = getNumberTexture(digit03)
        self.digitTexture04.texture = getNumberTexture(digit04)
        self.digitTexture05.texture = getNumberTexture(digit05)
        self.digitTexture06.texture = getNumberTexture(digit06)
        self.digitTexture07.texture = getNumberTexture(digit07)

        let scaleUp = SKAction.scaleTo(self.updateScale, duration: 0.03)
        let scaleDown = SKAction.scaleTo(self.defaultScale, duration: 0.1)
        let sequence = SKAction.sequence([scaleUp, scaleDown])
        
        self.digitTexture01.runAction(sequence)
        self.digitTexture02.runAction(sequence)
        self.digitTexture03.runAction(sequence)
        self.digitTexture04.runAction(sequence)
        self.digitTexture05.runAction(sequence)
        self.digitTexture06.runAction(sequence)
        self.digitTexture07.runAction(sequence)
    }
    
    private func getNumberTexture(number: Int) -> SKTexture {
        var texture: SKTexture
        switch number {
        case 0:
            texture = numberTexture00
        case 1:
            texture = numberTexture01
        case 2:
            texture = numberTexture02
        case 3:
            texture = numberTexture03
        case 4:
            texture = numberTexture04
        case 5:
            texture = numberTexture05
        case 6:
            texture = numberTexture06
        case 7:
            texture = numberTexture07
        case 8:
            texture = numberTexture08
        case 9:
            texture = numberTexture09
        default:
            texture = numberTexture00
        }
        return texture
    }
}

