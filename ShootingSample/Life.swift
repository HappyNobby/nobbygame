//
//  Life.swift
//  ShootingSample
//
//  Created by 大西信寛 on 2015/01/25.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import Foundation

import SpriteKit

class Life {
    let LIFE_MAX = 3
    private let LIFE_ON_TEXTURE = SKTexture(image: UIImage(named: "life_on.png")!)
    private let LIFE_OFF_TEXTURE = SKTexture(image: UIImage(named: "life_off.png")!)
    private let LIFE_ICON_SIZE: CGSize = CGSize(width: 25, height: 25)
    private var value: Int = 0
    var totalNode: SKNode = SKNode()
    private var life01 = SKSpriteNode()
    private var life02 = SKSpriteNode()
    private var life03 = SKSpriteNode()
    
    init() {
        self.value = self.LIFE_MAX
        self.life01.size = LIFE_ICON_SIZE
        self.life02.size = LIFE_ICON_SIZE
        self.life03.size = LIFE_ICON_SIZE
        self.life01.position = CGPoint(x: 0, y: 0)
        self.life02.position = CGPoint(x: self.LIFE_ICON_SIZE.width, y: 0)
        self.life03.position = CGPoint(x: self.LIFE_ICON_SIZE.width * 2, y: 0)
        self.life01.texture = LIFE_ON_TEXTURE
        self.life02.texture = LIFE_ON_TEXTURE
        self.life03.texture = LIFE_ON_TEXTURE
        self.totalNode.addChild(life01)
        self.totalNode.addChild(life02)
        self.totalNode.addChild(life03)
    }
    
    func setPos(position: CGPoint) {
        self.totalNode.position = position
    }
    func setValue(value: Int) {
        if (value < 0 || value > 3) {
            return
        }
        self.value = value
        self.totalNode.removeAllChildren()
        switch value {
        case 0:
            self.life01.texture = LIFE_OFF_TEXTURE
            self.life02.texture = LIFE_OFF_TEXTURE
            self.life03.texture = LIFE_OFF_TEXTURE
            self.totalNode.addChild(life01)
            self.totalNode.addChild(life02)
            self.totalNode.addChild(life03)
        case 1:
            self.life01.texture = LIFE_ON_TEXTURE
            self.life02.texture = LIFE_OFF_TEXTURE
            self.life03.texture = LIFE_OFF_TEXTURE
            self.totalNode.addChild(life01)
            self.totalNode.addChild(life02)
            self.totalNode.addChild(life03)
        case 2:
            self.life01.texture = LIFE_ON_TEXTURE
            self.life02.texture = LIFE_ON_TEXTURE
            self.life03.texture = LIFE_OFF_TEXTURE
            self.totalNode.addChild(life01)
            self.totalNode.addChild(life02)
            self.totalNode.addChild(life03)
        case 3:
            self.life01.texture = LIFE_ON_TEXTURE
            self.life02.texture = LIFE_ON_TEXTURE
            self.life03.texture = LIFE_ON_TEXTURE
            self.totalNode.addChild(life01)
            self.totalNode.addChild(life02)
            self.totalNode.addChild(life03)
        default:
            return
        }
    }
    func addValue(addValue: Int) {
        let afterValue = self.value + addValue
        self.setValue(afterValue)
    }
    func getValue() -> Int {
        return self.value
    }
}

