//
//  SceneEscapeProtocol.swift
//  ShootingSample
//
//  Created by Nobuhiro on 2015/01/06.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import SpriteKit

protocol SceneEscapeProtocol {
    
    func sceneEscape(scene: SKScene)
    
}
