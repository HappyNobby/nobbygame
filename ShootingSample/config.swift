//
//  config.swift
//  ShootingSample
//
//  Created by Nobuhiro on 2015/01/12.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import Foundation
import SpriteKit

let baseColor: UIColor = UIColor(red: 0.91, green: 0.90, blue: 0.90, alpha: 1.0)

// for Result Add
let INTER_SITIAL_INTERVAL: Int = 3
var resultInsAddDispCounter: Int = 0

// category for contact & collision
let enemyCategory: UInt32  = 1 << 0
let playerCategory: UInt32 = 1 << 1
let bulletCategory: UInt32 = 1 << 2
let scoreCategory: UInt32 = 1 << 3

// game setting
let LIFE_DEFAULT = 3
var didDiplyaedIntroFlg: Bool = false

// enemy
let enemyTexture02 = SKTexture(imageNamed: "combatto02.png")
let enemyTexture01 = SKTexture(imageNamed: "combatto01.png")

// gage
//let gageIconTexture = SKTexture(imageNamed: "gageIcon.png")
//let gageMaxIconTexture = SKTexture(imageNamed: "gageMaxIcon.png")
//let gageBarFrameTexture = SKTexture(imageNamed: "gage00.png")
let gageIconTexture = SKTexture(image: UIImage(named: "gageIcon.png")!)
let gageMaxIconTexture = SKTexture(image: UIImage(named: "gageMaxIcon.png")!)
let gageBarFrameTexture = SKTexture(image: UIImage(named: "gage00")!)

// 背景
let backgroundTexture = SKTexture(image: UIImage(named: "back")!)

// score, combo number
let numberTexture00 = SKTexture(imageNamed: "number0.png")
let numberTexture01 = SKTexture(imageNamed: "number1.png")
let numberTexture02 = SKTexture(imageNamed: "number2.png")
let numberTexture03 = SKTexture(imageNamed: "number3.png")
let numberTexture04 = SKTexture(imageNamed: "number4.png")
let numberTexture05 = SKTexture(imageNamed: "number5.png")
let numberTexture06 = SKTexture(imageNamed: "number6.png")
let numberTexture07 = SKTexture(imageNamed: "number7.png")
let numberTexture08 = SKTexture(imageNamed: "number8.png")
let numberTexture09 = SKTexture(imageNamed: "number9.png")

let targetMarkerTexture = SKTexture(imageNamed: "marker.png")


