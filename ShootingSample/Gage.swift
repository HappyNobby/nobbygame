//
//  Gage.swift
//  ShootingSample
//
//  Created by Nobuhiro on 2015/01/12.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import SpriteKit

// texture
let gageBarTexture01 = SKTexture(imageNamed: "gage01.png")
let gageBarTexture02 = SKTexture(imageNamed: "gage02.png")
let gageBarTexture03 = SKTexture(imageNamed: "gage03.png")
let gageBarTexture04 = SKTexture(imageNamed: "gage04.png")
let maxBarTexture01 = SKTexture(imageNamed: "gageMax01.png")
let maxBarTexture02 = SKTexture(imageNamed: "gageMax02.png")
let maxBarTexture03 = SKTexture(imageNamed: "gageMax03.png")
let maxBarTexture04 = SKTexture(imageNamed: "gageMax04.png")

class Gage: SKSpriteNode {
    let maxValue = 15
    private var value = 0
    private var isMax: Bool = false
    let barFrameSize = CGSize(width: 100, height: 20)
    
    override init(texture: SKTexture! = gageBarTexture01, color: UIColor! = nil, size: CGSize = CGSize(width: 100.0, height: 100.0)) {
        super.init(texture: texture, color: color, size: size)
        self.size = CGSize(width: 0, height: self.barFrameSize.height)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // gageBarの長さ更新
    func setValue(value: Int) {
        if (value >= self.maxValue) {
            self.value = maxValue
            self.size.width = self.barFrameSize.width
            self.setMax(true)
            self.flashBarAnimation(true)
        } else {
            self.value = value
            var scale = CGFloat(value) / CGFloat(maxValue)
            if (scale > 1.0) {
                scale = 1.0
            }
            self.size.width = self.barFrameSize.width * scale
            self.flashBarAnimation(false)
        }
    }
    func addValue(value: Int) {
        setValue(self.value + value)
    }
    
    func flashBarAnimation(isMax: Bool) {
        var flashAction: SKAction
        if (isMax) {
            let anim = SKAction.animateWithTextures([maxBarTexture01, maxBarTexture02, maxBarTexture03, maxBarTexture04, maxBarTexture01], timePerFrame: 0.05)
            flashAction = SKAction.repeatActionForever(anim)
        } else {
            let anim = SKAction.animateWithTextures([gageBarTexture01, gageBarTexture02, gageBarTexture03, gageBarTexture04, gageBarTexture01], timePerFrame: 0.03)
            flashAction = SKAction.repeatAction(anim, count: 1)
        }
        self.runAction(flashAction, withKey: "flash")
    }
    
    func setMax(flag: Bool) {
        self.isMax = flag
    }
    func isGageFull() -> Bool {
        return self.isMax
    }
    func clear() {
        self.setMax(false)
        self.removeActionForKey("flash")
        self.setValue(0)
    }
}
