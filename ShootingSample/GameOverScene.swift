//
//  GameOverScene.swift
//  ShootingSample
//
//  Created by Nobuhiro on 2015/01/10.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import SpriteKit

class GameOverScene: SKScene {
    var delegate_escape: SceneEscapeProtocol?
    
    override func didMoveToView(view: SKView) {
        // background color
        self.backgroundColor = UIColor.blackColor()
        
        // text label
        let gameOverLabel = SKLabelNode()
        gameOverLabel.text = "Game Over"
        gameOverLabel.fontSize = 50
        gameOverLabel.fontColor = UIColor.whiteColor()
        gameOverLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        
        let scoreLabel = SKLabelNode()
        scoreLabel.text = "9999999"
        scoreLabel.fontSize = 30
        scoreLabel.fontColor = UIColor.whiteColor()
        scoreLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) - 60)
        
        self.addChild(gameOverLabel)
        self.addChild(scoreLabel)
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        delegate_escape!.sceneEscape(self)
    }
}

