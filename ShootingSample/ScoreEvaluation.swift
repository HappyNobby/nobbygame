//
//  ScoreEvaluation.swift
//  ShootingSample
//
//  Created by 大西信寛 on 2015/01/31.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import Foundation

func getResultMessage(score: Int) ->String {
    let index = Int(arc4random_uniform(UInt32(3)))
    var message: String = ""
    
    if (score < 1000) {
        // level1 0 - 999
        message = eval_level1[index]
    } else if (score < 3000) {
        // level2 1000 - 2999
        message = eval_level2[index]
    } else if (score < 5000) {
        // level3 3000 - 4999
        message = eval_level3[index]
    } else if (score < 10000) {
        // level4 5000 - 9999
        message = eval_level4[index]
    } else if (score < 15000) {
        // level5 10000 - 14999
        message = eval_level5[index]
    } else if (score < 20000) {
        // level6 15000 - 19999
        message = eval_level6[index]
    } else if (score < 25000) {
        // level7 20000 - 24999
        // 結構MAX
        message = eval_level7[index]
    } else if (score < 30000) {
        // level8 25000 - 29999
        // 超うまい
        message = eval_level8[index]
    } else if (score < 40000) {
        // level9 30000 - 39999
        // 限りなく神
        message = eval_level9[index]
        
    } else {
        if (score >= 9999999) {
            // 最高スコア
            message = "信じられないことが起きました"
        }
        else {
            // level10 40000 - 9999998
            // もうおかしい人
            message = eval_level10[index]
        }
    }
    return message
}

// MAX44文字
// あいうえおかきくけこさしすせそたちつてとなにぬね
// level1 0 - 999
let eval_level1: [String] = [
    "守る気はありますか？",
    "全員やられてしまった。。一瞬だった",
    "焦る必要はない。落ち着いて、狙う"]

// level2 1000 - 2999
let eval_level2: [String] = [
    "リズムをつかめるかどうか",
    "防衛のセンスがある・・とは言えない",
    "全員やられてしまった。。それでも前を向く"]

// level3 3000 - 4999
let eval_level3: [String] = [
    "もう少しだ。もう少し、力がほしい",
    "とことん信じ込む。そうすれば、できる",
    "あきらめなければ終わらないんだ"]

// level4 5000 - 9999
let eval_level4: [String] = [
    "なんとか全員逃げ切れたようだ",
    "少しは強くなれたのだろうか・・・",
    "休んでいる暇はない。大切なものを守るために"]

// level5 10000 - 14999
let eval_level5: [String] = [
    "この世界が好きだ。戦うのが好きなわけじゃない",
    "疲れてきたら必殺ショットで休憩するのも1つ",
    "どこまでも強くなれる気がする"]

// level6 15000 - 19999
let eval_level6: [String] = [
    "とてもいい感じだ",
    "防衛のセンスが・・・感じられる",
    "安心して前を向ける"]

// level7 20000 - 24999
let eval_level7: [String] = [
    "すばらしい防衛に感謝",
    "あなたのおかげで平和なのだろう",
    "敵もなすすべがないだろう"]

// level8 25000 - 29999
let eval_level8: [String] = [
    "間違いない。壁を越えている",
    "すばらしい防衛どころの騒ぎではない",
    "敵も見とれているのではないか？"]

// level9 30000 - 39999
let eval_level9: [String] = [
    "防衛、極まる",
    "神ここにあり",
    "守護神"]

// level10 40000 - 9999998
let eval_level10: [String] = [
    "ここまで極めてくださりありがとうございます",
    "もう開発者よりうまいです。",
    "ここまで極めてくださりありがとうございます"]
