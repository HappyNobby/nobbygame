//
//  GameViewController.swift
//  ShootingSample
//
//  Created by Nobuhiro on 2015/01/05.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import UIKit
import SpriteKit


extension SKNode {
    class func unarchiveFromFile(file : NSString) -> SKNode? {
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks") {
            var sceneData = NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe, error: nil)!
            var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as GameScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
    }
}


class GameViewController: UIViewController, SceneEscapeProtocol {
    var skView: SKView?
    var score = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        skView = self.view as? SKView
        
        self.skView = self.view as? SKView
        //skView!.showsFPS = true
        //skView!.showsNodeCount = true
        //skView!.showsDrawCount = true
        skView!.ignoresSiblingOrder = true

        /*
        if let scene = GameScene.unarchiveFromFile("GameScene") as? GameScene {
            // Configure the view.
            skView = self.view as? SKView
            skView!.showsFPS = true
            skView!.showsNodeCount = true
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView!.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            scene.delegate_escape = self
            skView!.presentScene(scene)
        }
        */
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        goGameScene()
    }

    

    func goGameScene() {
        if (self.skView?.scene != nil) {
            self.skView?.scene?.removeAllChildren()
            self.skView?.presentScene(self.skView?.scene)
        } else {
            let gameScene = GameScene(size: UIScreen.mainScreen().applicationFrame.size)
            gameScene.delegate_escape = self
            gameScene.scaleMode = SKSceneScaleMode.AspectFill
            self.skView!.presentScene(gameScene)
        }
    }
    
    func sceneEscape(scene: SKScene) {
        var resultVc = self.storyboard?.instantiateViewControllerWithIdentifier("resultView") as? ResultViewController
        resultVc?.score = NSUserDefaults.standardUserDefaults().integerForKey("score")
        self.navigationController?.pushViewController(resultVc!, animated: true)
        
        
        /*
        if scene.isKindOfClass(TitleScene) {
            goGameScene()
        } else if scene.isKindOfClass(GameScene) {
            //goGameOverScene()
            //self.dismissViewControllerAnimated(true, completion: nil)
            let resultViewController = self.storyboard?.instantiateViewControllerWithIdentifier("resultView") as UIViewController
            self.presentViewController(resultViewController, animated: true, completion: nil)
            
        } else if scene.isKindOfClass(GameOverScene) {
            goTitleScene()
        }
        */
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> Int {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return Int(UIInterfaceOrientationMask.AllButUpsideDown.rawValue)
        } else {
            return Int(UIInterfaceOrientationMask.All.rawValue)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
