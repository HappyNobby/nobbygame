//
//  SpecialShotButton.swift
//  ShootingSample
//
//  Created by 大西信寛 on 2015/01/18.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import SpriteKit

let SPECIAL_SHOT_BUTTON_RADIUS: CGFloat = 20.0
let SPECIAL_SHOT_BUTTON_FONTSIZE: CGFloat = 20.0

class SpecialShotButton {
    // unrecognized selector sent
    var button: SKShapeNode!
    private var isMax: Bool = false
    private let path = NSBundle.mainBundle().pathForResource("smoke", ofType: "sks")
    
    init() {
        isMax = false
        button = SKShapeNode()
        var circlePath = CGPathCreateMutable()
        var angle = CGFloat(M_PI * 2.0)
        CGPathAddArc(circlePath, nil, 0, 0, SPECIAL_SHOT_BUTTON_RADIUS, 0, angle, true)
        button.path = circlePath
        button.fillColor = UIColor.darkGrayColor()
        button.strokeColor = UIColor.clearColor()
        
        var label: SKLabelNode = SKLabelNode(fontNamed: "MarkerFelt-Wide")
        label.text = "技"
        label.fontSize = SPECIAL_SHOT_BUTTON_FONTSIZE
        label.fontColor = UIColor.whiteColor()
        label.position = CGPoint(x: 0, y: -7)
        button.addChild(label)
    }
    
    func setPos(position: CGPoint) {
        self.button.position = position
    }
    
    func setMax(flag: Bool) {
        if (self.isMax == flag) {
            // すでに指定したflg状態の場合は何もしない
            return
        }
        if (flag) {
            self.isMax = true
            button.fillColor = UIColor.orangeColor()
            button.removeAllChildren()
            
            var label: SKLabelNode = SKLabelNode(fontNamed: "MarkerFelt-Wide")
            label.text = "技"
            label.fontSize = SPECIAL_SHOT_BUTTON_FONTSIZE
            label.fontColor = UIColor.blackColor()
            label.position = CGPoint(x: 0, y: -7)
            
            var effect = NSKeyedUnarchiver.unarchiveObjectWithFile(path!) as SKEmitterNode
            label.addChild(effect)
            button.addChild(label)
            
            var tapHere: SKLabelNode = SKLabelNode(fontNamed: "MarkerFelt-Wide")
            tapHere.text = "tap circle!"
            tapHere.fontSize = 20
            tapHere.fontColor = UIColor.orangeColor()
            tapHere.position = CGPoint(x: 0, y: 30)
            let fadeIn = SKAction.fadeInWithDuration(0.3)
            let fadeOut = SKAction.fadeOutWithDuration(1.0)
            let sequence = SKAction.sequence([fadeIn, fadeOut])
            let anim = SKAction.repeatActionForever(sequence)
            tapHere.runAction(anim)
            button.addChild(tapHere)
        } else {
            self.isMax = false
            button.fillColor = UIColor.darkGrayColor()
            button.removeAllChildren()
            
            var label: SKLabelNode = SKLabelNode(fontNamed: "MarkerFelt-Wide")
            label.text = "技"
            label.fontSize = SPECIAL_SHOT_BUTTON_FONTSIZE
            label.fontColor = UIColor.whiteColor()
            label.position = CGPoint(x: 0, y: -7)
            button.addChild(label)
        }
    }
    
    func isPushable() -> Bool {
        return self.isMax
    }
}
