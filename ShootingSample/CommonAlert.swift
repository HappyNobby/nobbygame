//
//  CommonAlert.swift
//  ShootingSample
//
//  Created by 大西信寛 on 2015/01/31.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import Foundation

@objc protocol CommonAlertDelegate {
    optional func buttonTapped(tag: Int)
}

// iOS8以前とiOS8以降両対応アラートメソッド
class CommonAlert {
    var delegate : CommonAlertDelegate!
    
    init() {
    }
    
    init(delegate: CommonAlertDelegate) {
        self.delegate = delegate
    }
    
    func showAlert(viewController: UIViewController, title: String = "Notice", buttonTitle: String = "OK", message: String, tag: Int = 0) {
        if ( NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 ) {
            // iOS 8 ~
            var alert: UIAlertController?
            alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let afterAction = UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.Default) {
                action in
                if self.delegate != nil {
                    self.delegate.buttonTapped!(tag)
                }
            }
            alert!.addAction(afterAction)
            viewController.presentViewController(alert!, animated: true, completion: nil)
        } else {
            // ~ iOS7
            var alert: UIAlertView = UIAlertView()
            alert.delegate = viewController
            alert.title = title
            alert.message = message
            alert.tag = tag
            alert.addButtonWithTitle("OK")
            alert.show()
        }
    }
    
}