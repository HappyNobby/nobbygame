//
//  Player.swift
//  ShootingSample
//
//  Created by 大西信寛 on 2015/01/18.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import SpriteKit

enum PLAYER_STATE : Int {
    case NORMAL = 0
    case CHARGE = 1
    case SHOT = 2
    case SPECIAL_SHOT = 3
}

class Player {
    /*
    let playerTexture = SKTexture(imageNamed: "player_shot.png")
    let playerShotTexture01 = SKTexture(imageNamed: "player_shot01.png")
    let playerShotTexture02 = SKTexture(imageNamed: "player_shot02.png")
    let playerShotTexture03 = SKTexture(imageNamed: "player_shot03.png")
    let playerShotTexture04 = SKTexture(imageNamed: "player_shot04.png")
    let playerShotTexture05 = SKTexture(imageNamed: "player_shot05.png")
    let playerShotTexture06 = SKTexture(imageNamed: "player_shot06.png")
    let playerShotTexture07 = SKTexture(imageNamed: "player_shot07.png")
    let playerChargeTexture01 = SKTexture(imageNamed: "charge01.png")
    let playerChargeTexture02 = SKTexture(imageNamed: "charge02.png")
    */

    let playerShotTexture01 = SKTexture(image: UIImage(named: "eiden01.png")!)
    let playerShotTexture02 = SKTexture(image: UIImage(named: "eiden02.png")!)
    let playerShotTexture03 = SKTexture(image: UIImage(named: "eiden03.png")!)
    let playerShotTexture04 = SKTexture(image: UIImage(named: "eiden04.png")!)
    let playerShotTexture05 = SKTexture(image: UIImage(named: "eiden05.png")!)
    let playerShotTexture06 = SKTexture(image: UIImage(named: "eiden06.png")!)
    let playerShotTexture07 = SKTexture(image: UIImage(named: "eiden07.png")!)
    let playerSpecialTexture01 = SKTexture(image: UIImage(named: "eiden_special.png")!)
    let playerChargeTexture01 = SKTexture(image: UIImage(named: "charge01.png")!)
    let playerChargeTexture02 = SKTexture(image: UIImage(named: "charge02.png")!)
    
    private var playerState: PLAYER_STATE = PLAYER_STATE.NORMAL
    
    private let PLAYER_BASE_POS: CGPoint = CGPointMake(52, 100)
    private let PLAYER_SIZE: CGSize = CGSize(width: 100, height: 100)
    private let CHARGE_POS: CGPoint = CGPointMake(45, 122)
    private let CHARGE_SIZE: CGSize = CGSize(width: 120, height: 160)
    
    private var player: SKSpriteNode!
    private var charge: SKSpriteNode! // charge stateでplayerの周囲に表示させるエフェクト用
    
    init(scene: SKScene) {
        self.player = SKSpriteNode(texture: playerShotTexture01)
        self.player.size = PLAYER_SIZE
        self.player.position = PLAYER_BASE_POS
        self.player.physicsBody = SKPhysicsBody(circleOfRadius: player.size.height / 2.0)
        self.player.physicsBody?.dynamic = false
        self.player.physicsBody?.allowsRotation = false
        self.player.physicsBody?.dynamic = false
        self.player.physicsBody?.allowsRotation = false
        
        // 衝突・接触設定(playerは敵とのみ接触判定。衝突判定はなし)
        self.player.physicsBody?.categoryBitMask = playerCategory
        self.player.physicsBody?.contactTestBitMask = enemyCategory
        self.player.physicsBody?.collisionBitMask = 0
        
        scene.addChild(player)
        
        self.charge = SKSpriteNode(texture: playerChargeTexture01)
        self.charge.position = CHARGE_POS
        self.charge.size = CHARGE_SIZE
        let anim = SKAction.animateWithTextures([playerChargeTexture01, playerChargeTexture02], timePerFrame: 0.08)
        let chargeAction = SKAction.repeatActionForever(anim)
        self.charge.runAction(chargeAction)
        self.charge.alpha = 0
        
        scene.addChild(charge)
    }
    
    private func setPlayerAnimationCharge() {
        if (self.playerState == PLAYER_STATE.CHARGE) {
            return
        }
        self.charge.alpha = 1.0
    }
    
    private func setPlayerAnimationShot() {
        // 0.025
        let anim = SKAction.animateWithTextures([playerShotTexture01, playerShotTexture02, playerShotTexture03, playerShotTexture04, playerShotTexture05, playerShotTexture06, playerShotTexture07, playerShotTexture01], timePerFrame: 0.03)
        let shot = SKAction.repeatAction(anim, count: 1)
        self.player.runAction(shot)
    }
    
    func setPlayerState(state: PLAYER_STATE) {
        switch state {
        case PLAYER_STATE.NORMAL:
            self.playerState = PLAYER_STATE.NORMAL
            self.player.texture = playerShotTexture01
            self.charge.alpha = 0
        case PLAYER_STATE.CHARGE:
            self.playerState = PLAYER_STATE.CHARGE
            self.charge.alpha = 1.0
            self.setPlayerAnimationCharge()
        case PLAYER_STATE.SHOT:
            self.playerState = PLAYER_STATE.SHOT
            self.charge.alpha = 0
            self.setPlayerAnimationShot()
        case PLAYER_STATE.SPECIAL_SHOT:
            self.playerState = PLAYER_STATE.SPECIAL_SHOT
            self.player.texture = self.playerSpecialTexture01
        default:
            return
        }
    }
    
    func getPlayerState() -> PLAYER_STATE {
        return self.playerState
    }
    
    func getPlayerPos() -> CGPoint {
        return PLAYER_BASE_POS
    }
    
    func fixPlayerPos() {
        self.player.position = PLAYER_BASE_POS
    }
}
