//
//  ComboManager.swift
//  ShootingSample
//
//  Created by Nobuhiro on 2015/01/16.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

//import UIKit
import SpriteKit

class ComboManager {
    let DIGIT = 2
    let MAX_VALUE = 999
    
    enum COMBO_LEVEL : Int {
        case COMBO_LEVEL_0 = 0
        case COMBO_LEVEL_1 = 1
        case COMBO_LEVEL_2 = 2
        case COMBO_LEVEL_3 = 3
        
        func toInt() -> Int {
            switch self {
            case COMBO_LEVEL.COMBO_LEVEL_0:
                return 0
            case COMBO_LEVEL.COMBO_LEVEL_1:
                return 10
            case COMBO_LEVEL.COMBO_LEVEL_2:
                return 20
            case COMBO_LEVEL.COMBO_LEVEL_3:
                return 30
            default:
                return 0
            }
        }
        func getFontScale() -> CGFloat {
            switch self {
            case COMBO_LEVEL.COMBO_LEVEL_0:
                return 1.0
            case COMBO_LEVEL.COMBO_LEVEL_1:
                return 1.5
            case COMBO_LEVEL.COMBO_LEVEL_2:
                return 2.0
            case COMBO_LEVEL.COMBO_LEVEL_3:
                return 2.3
            default:
                return 1.0
            }
        }
        
        func getMaxFontScale() -> CGFloat {
            switch self {
            case COMBO_LEVEL.COMBO_LEVEL_0:
                return 1.5
            case COMBO_LEVEL.COMBO_LEVEL_1:
                return 2.0
            case COMBO_LEVEL.COMBO_LEVEL_2:
                return 2.5
            case COMBO_LEVEL.COMBO_LEVEL_3:
                return 2.7
            default:
                return 1.5
            }
        }
        
        func getColor() -> UIColor {
            switch self {
            case COMBO_LEVEL.COMBO_LEVEL_0:
                return UIColor.blackColor()
            case COMBO_LEVEL.COMBO_LEVEL_1:
                return UIColor(red: 1.0, green: 0.73, blue: 0.06, alpha: 1.0)
            case COMBO_LEVEL.COMBO_LEVEL_2:
                return UIColor(red: 1.0, green: 0.73, blue: 0.06, alpha: 1.0)
            case COMBO_LEVEL.COMBO_LEVEL_3:
                return UIColor(red: 1.0, green: 0.19, blue: 0.19, alpha: 1.0)
            default:
                return UIColor.blackColor()
            }
        }
    }
    let DEFAULT_FONT_SIZE:CGFloat = 25.0
    
    init() {}
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func showComboNum(scene: SKScene, position: CGPoint, value: Int) {
        var dispValue = value < 0 ? 0 : value
        dispValue = value > self.MAX_VALUE ? self.MAX_VALUE : value
        var dispPos: CGPoint = CGPointMake(position.x, position.y)
        // コンボ数表示位置の補正(画面外に切れる場合は補正する)
        if (position.x < 15) {dispPos.x = 15}
        if (position.x > scene.frame.size.width) {dispPos.x = scene.frame.size.width - 15}
        if (position.y > scene.frame.size.height) {dispPos.x = scene.frame.size.height - 15}
        
        
        var level: COMBO_LEVEL = COMBO_LEVEL.COMBO_LEVEL_0
        
        if (dispValue < COMBO_LEVEL.COMBO_LEVEL_1.toInt()) {
            level = COMBO_LEVEL.COMBO_LEVEL_0
        } else if (dispValue < COMBO_LEVEL.COMBO_LEVEL_2.toInt()) {
            level = COMBO_LEVEL.COMBO_LEVEL_1
        } else if (dispValue < COMBO_LEVEL.COMBO_LEVEL_3.toInt()) {
            level = COMBO_LEVEL.COMBO_LEVEL_2
        } else {
            level = COMBO_LEVEL.COMBO_LEVEL_3
        }
        var combo: SKLabelNode = SKLabelNode(fontNamed: "MarkerFelt-Wide")
        combo.fontColor = level.getColor()
        combo.fontSize = 30.0
        combo.position = dispPos
        combo.zPosition = 100
        combo.text = String(dispValue)
        
        scene.addChild(combo)
        
        let scaleUp   = SKAction.scaleTo(level.getMaxFontScale(), duration: 0.1)
        let scaleDown = SKAction.scaleTo(level.getFontScale(), duration: 0.2)
        let fadeOut = SKAction.fadeOutWithDuration(0.3)
        let remove = SKAction.removeFromParent()
        let sequence = SKAction.sequence([scaleUp, scaleDown, fadeOut, remove])
        combo.runAction(sequence)
    }
    
    
}


