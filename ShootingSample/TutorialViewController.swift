//
//  TutorialViewController.swift
//  ShootingSample
//
//  Created by Nobuhiro on 2015/01/11.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController, UIScrollViewDelegate {
    let frameSize = UIScreen.mainScreen().applicationFrame.size
    var scrView = UIScrollView()
    @IBOutlet weak var pageControl: UIPageControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // tutorial用画像
        let img1 = UIImage(named: "tutorial01.png")
        let img2 = UIImage(named: "tutorial02.png")
        let img3 = UIImage(named: "tutorial03.png")
        
        //UIImageViewにUIIimageを追加
        var imageView1 = UIImageView(image:img1)
        let imageView2 = UIImageView(image:img2)
        let imageView3 = UIImageView(image:img3)
        
        scrView.delegate = self
        //表示位置 + 1ページ分のサイズ
        scrView.frame = CGRectMake(0, 100, frameSize.width, frameSize.height - 100)
        
        //全体のサイズ
        scrView.contentSize = CGSizeMake(frameSize.width * 3, frameSize.height - 100)
        //左右に並べる
        imageView1.frame = CGRectMake(0, 0, frameSize.width, frameSize.height - 100)
        imageView2.frame = CGRectMake(frameSize.width * 1, 0, frameSize.width, frameSize.height - 100)
        imageView3.frame = CGRectMake(frameSize.width * 2, 0, frameSize.width, frameSize.height - 100)
        
        //viewに追加
        self.view.addSubview(scrView)
        scrView.addSubview(imageView1)
        scrView.addSubview(imageView2)
        scrView.addSubview(imageView3)
        
        // １ページ単位でスクロールさせる
        scrView.pagingEnabled = true
        
        //scroll画面の初期位置
        scrView.contentOffset = CGPointMake(0, 0);

        // Do any additional setup after loading the view.
        self.view.backgroundColor = baseColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        var pageWidth : CGFloat = self.scrView.frame.size.width
        var fractionalPage : CGFloat = self.scrView.contentOffset.x / pageWidth
        
        let page : Int = lround(Double(fractionalPage))
        self.pageControl.currentPage = page
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backButtonPushed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
