//
//  GameScene.swift
//  ShootingSample
//
//  Created by Nobuhiro on 2015/01/05.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    var delegate_escape: SceneEscapeProtocol?
    var bannerView: GADBannerView?
    var interstitial: GADInterstitial?
    
    // player
    var player: Player!
    // enemy
    let ENEMY_SIZE: CGSize = CGSize(width: 60, height: 60)
    var enemyStattPositions: [CGPoint] = []
    
    // for game level
    var enemyAddInterval: Int = 240
    let ENEMY_INTERVAL_MAX: Int = 100
    let ENEMY_INTERVAL_MIN_1: Int = 60
    let ENEMY_INTERVAL_MIN_2: Int = 45
    let ENEMY_INTERVAL_MIN_3: Int = 35
    var enemySpeed: CGFloat = -100
    let ENEMY_SPEED_MAX: CGFloat = -500
    let ENEMY_SPEED_MIN: CGFloat = -100
    var enemyCrashCount: Int = 0
    var totalEnemyCrashCount: Int = 0
    let ENEMY_CRASH_LEVEL1: Int = 15
    let ENEMY_CRASH_LEVEL2: Int = 70
    let ENEMY_CRASH_LEVEL3: Int = 100
    let ENEMY_CRASH_LEVEL4: Int = 180
    var enemyCrashNumUnit: Int = 1
    
    // bullet
    let BULLET_SPEED: Double = 1500
    let BULLET_MIN: CGFloat = 10
    let BULLET_MAX: CGFloat = 100
    var bulletSize: CGFloat = 10
    var targetMarker: SKSpriteNode!
    
    // score
    var score = Score()
    var scoreText: SKSpriteNode!
    // combo
    var comboNum = NSInteger()
    var comboManager: ComboManager = ComboManager()
    // gage
    var gage: Gage = Gage()
    var gageBarFrame: SKSpriteNode!
    
    // specialShot
    var specialShotButton: SpecialShotButton = SpecialShotButton()
    // life
    var life: Life = Life()
    
    let INTERVAL_TO_GAME_START = 100
    var timer = 0
    
    enum GAME_STATE : Int {
        case BEFORE_START = 0
        case NORMAL = 1
        case STOP = 2
        case SPECAIL_SHOT_BEGIN = 3
        case SPECAIL_SHOT_END = 4
        case GAME_OVER = 5
    }
    var gameState: GAME_STATE = GAME_STATE.BEFORE_START
    
    // MARK: didMoveToView
    override func didMoveToView(view: SKView) {
        // ----- フッター広告表示------
        var footerFrame = SKShapeNode()
        var rect = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: CGRectGetMaxX(self.frame), height: 50))
        footerFrame.path = CGPathCreateWithRect(rect, nil)
        footerFrame.fillColor = UIColor.darkGrayColor()
        footerFrame.strokeColor = UIColor.blackColor()
        self.addChild(footerFrame)
        
        var offsetX = (CGRectGetMaxX(self.frame) - kGADAdSizeBanner.size.width) / 2
        bannerView = GADBannerView(adSize: kGADAdSizeBanner, origin: CGPointMake(offsetX, CGRectGetMaxY(self.frame) - 50))
        bannerView?.adUnitID = "ca-app-pub-8756306138420194/4903153661"
        bannerView?.rootViewController = UIApplication.sharedApplication().keyWindow?.rootViewController
        self.view?.addSubview(bannerView!)
        let request = GADRequest()
        //request.testDevices = [ "0c564c011ea6e55b1c06ec5b17863b8a", GAD_SIMULATOR_ID ]
        bannerView?.loadRequest(request)
        
        // 背景
        var background = SKSpriteNode(texture: backgroundTexture)
        background.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        background.size = self.frame.size
        background.alpha = 0.15
        background.zPosition = -10
        self.addChild(background)
        
        self.backgroundColor = baseColor
        
        // setup physics
        self.physicsWorld.gravity = CGVectorMake(0.0, 0.0)
        self.physicsWorld.contactDelegate = self
        
        // player
        self.player = Player(scene: self)
        
        // bullet
        self.bulletSize = BULLET_MIN
        self.targetMarker = SKSpriteNode(texture: targetMarkerTexture)
        self.targetMarker.size = CGSizeMake(BULLET_MIN, BULLET_MIN)
        self.addChild(targetMarker)
        self.targetMarker.alpha = 0
        
        // score & scoreText
        self.score.setScore(0)
        self.scoreText = SKSpriteNode(texture: SKTexture(image: UIImage(named: "score_text.png")!))
        self.scoreText.size = CGSize(width: 60, height: 15)
        self.scoreText.position = CGPointMake(25, self.frame.height - 25)
        self.addChild(scoreText)
        self.score.position = CGPointMake(50, self.frame.height - 20)
        self.score.setScore(0)
        self.addChild(score)
        
        // combo
        self.comboNum = 0
        
        // gage
        gageBarFrame = SKSpriteNode(texture: gageBarFrameTexture)
        gageBarFrame.size = gage.barFrameSize
        gageBarFrame.anchorPoint = CGPointMake(0, 0.5) // 基準点をバーの左端に変更
        gageBarFrame.position = CGPoint(x: self.frame.width - 105, y: 75)
        gage.clear()
        gage.anchorPoint = CGPointMake(0, 0.5) // 基準点をバーの左端に変更
        gage.position = gageBarFrame.position
        self.addChild(gageBarFrame)
        self.addChild(gage)
        
        // special shot button
        let spBtnPos = CGPoint(x: self.frame.width - 125, y: 75)
        self.specialShotButton.setPos(spBtnPos)
        self.specialShotButton.setMax(false)
        self.addChild(specialShotButton.button)
        
        // life
        let lifePos = CGPointMake(self.frame.width - 80, self.frame.height - 20)
        self.life.setPos(lifePos)
        self.life.setValue(self.life.LIFE_MAX)
        self.addChild(self.life.totalNode)
        
        // Other initialization
        self.enemyAddInterval = self.ENEMY_INTERVAL_MAX
        self.enemySpeed = self.ENEMY_SPEED_MIN
        self.enemyCrashCount = 0
        self.totalEnemyCrashCount = 0
        self.enemyCrashNumUnit = 1
        self.view?.paused = false
        self.dicideEnemyPos()
        self.timer = 0
        self.gameState = GAME_STATE.BEFORE_START
        
        // game start!!
        if (!didDiplyaedIntroFlg) {
            self.showIntroText()
        } else {
            self.showStartText()
        }
        
    }
    
    // MARK: touches
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        if (self.gameState != GAME_STATE.NORMAL) {
            return
        }
        // タッチした位置にtargetMakerを表示
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            if (specialShotButton.isPushable() && specialShotButton.button.containsPoint(location)) {
                return
            }
            self.targetMarker.position = location
            self.targetMarker.size = CGSizeMake(BULLET_MIN, BULLET_MIN)
            self.targetMarker.alpha = 1.0
            
            if (self.player.getPlayerState() == PLAYER_STATE.NORMAL) {
                self.player.setPlayerState(PLAYER_STATE.CHARGE)
            }
        }
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        if (self.gameState != GAME_STATE.NORMAL) {
            return
        }
        if (self.player.getPlayerState() == PLAYER_STATE.CHARGE) {
            for touch: AnyObject in touches {
                let location = touch.locationInNode(self)
                self.targetMarker.position = location
            }
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        if (self.gameState != GAME_STATE.NORMAL) {
            return
        }
        for touch: AnyObject in touches {
            // specialShotButton判定
            let location = touch.locationInNode(self)
            if (specialShotButton.isPushable() && specialShotButton.button.containsPoint(location)) {
                self.player.setPlayerState(PLAYER_STATE.SPECIAL_SHOT)
                showSpecialText()
                self.specialShotButton.setMax(false)
                self.gage.clear()
                return
            }
            
            // タッチが離れた位置に向かって球を発射する
            var bullet = SKSpriteNode(texture: SKTexture(image: UIImage(named: "bullet.png")!))
            bullet.name = "bullet"
            bullet.size = CGSizeMake(self.bulletSize, self.bulletSize)
            bullet.position = self.player.getPlayerPos()
            bullet.physicsBody = SKPhysicsBody(circleOfRadius: bullet.size.height / 2.0)
            bullet.physicsBody?.dynamic = true
            bullet.physicsBody?.categoryBitMask = bulletCategory
            bullet.physicsBody?.collisionBitMask = 0
            bullet.physicsBody?.usesPreciseCollisionDetection = true
            self.addChild(bullet)
            self.targetMarker.position = location
            
            // 弾の移動方向を決定
            let v = sub(targetMarker.position, from: self.player.getPlayerPos())
            let rad = atan2(Double(v.y), Double(v.x))
            
            bullet.physicsBody?.velocity.dx = CGFloat(cos(rad) * BULLET_SPEED)
            bullet.physicsBody?.velocity.dy = CGFloat(sin(rad) * BULLET_SPEED)
            
            // マーカー非表示&チャージサイズ初期化
            self.targetMarker.alpha = 0
            self.bulletSize = BULLET_MIN
            
            self.player.setPlayerState(PLAYER_STATE.SHOT)
            // NORMAL状態に遷移
            self.player.setPlayerState(PLAYER_STATE.NORMAL)
        }
    }
    
    // MARK: update
    override func update(currentTime: CFTimeInterval) {
        if (self.gameState != GAME_STATE.NORMAL) {
            return
        }
        self.timer++
        self.player.fixPlayerPos()
        if (self.player.getPlayerState() == PLAYER_STATE.CHARGE) {
            // 弾とマーカーの大きさを大きくする
            if (self.bulletSize < BULLET_MAX) {
                self.bulletSize += 3
                self.targetMarker.size.width = self.bulletSize
                self.targetMarker.size.height = self.bulletSize
            }
        }
        if (self.timer != 0 && self.timer % self.enemyAddInterval == 0) {
            self.timer = 0
            // enemy追加
            var enemyV = CGVector(dx: self.enemySpeed, dy: 0)
            var rand1 = Int(arc4random_uniform(UInt32(5)))
            if (self.totalEnemyCrashCount < self.ENEMY_CRASH_LEVEL1) {
                // 同時出現数1
                self.addEnemy(self.enemyStattPositions[rand1], velocity: enemyV)
            } else {
                var indexArray: [Int] = [0,1,2,3,4]
                var rand2 = Int(arc4random_uniform(UInt32(4)))
                var rand3 = Int(arc4random_uniform(UInt32(3)))
                var index1 = indexArray[rand1]
                indexArray.removeAtIndex(rand1)
                var index2 = indexArray[rand2]
                indexArray.removeAtIndex(rand2)
                var index3 = indexArray[rand3]
                indexArray.removeAtIndex(rand3)
                
                var luck = Int(arc4random_uniform(UInt32(100))) % 2 == 0 ? true : false
                if (self.totalEnemyCrashCount < self.ENEMY_CRASH_LEVEL2) {
                    // 同時出現数1~2
                    self.addEnemy(self.enemyStattPositions[index1], velocity: enemyV)
                    if (luck) {
                        self.addEnemy(self.enemyStattPositions[index2], velocity: enemyV)
                    }
                } else {
                    // 同時出現数2~3
                    self.addEnemy(self.enemyStattPositions[index1], velocity: enemyV)
                    self.addEnemy(self.enemyStattPositions[index2], velocity: enemyV)
                    var luck2 = Int(arc4random_uniform(UInt32(100))) % 2 == 0 ? true : false
                    if (luck && luck2 && self.totalEnemyCrashCount > self.ENEMY_CRASH_LEVEL4) {
                        self.addEnemy(self.enemyStattPositions[index3], velocity: enemyV)
                    }
                }
            }
        }
    }
    
    // MARK: addEnemy
    func addEnemy(position: CGPoint, velocity: CGVector) {
        // NORMAL状態以外では、生成しない
        if (self.gameState != GAME_STATE.NORMAL) {
            return
        }
        var enemy = SKSpriteNode(texture: enemyTexture01)
        enemy.name = "enemy"
        enemy.size = CGSizeMake(60, 60)
        enemy.position = position
        
        enemy.physicsBody = SKPhysicsBody(circleOfRadius: 15)
        enemy.physicsBody?.dynamic = true
        
        enemy.physicsBody?.velocity = velocity
        
        enemy.physicsBody?.categoryBitMask = enemyCategory
        enemy.physicsBody?.collisionBitMask = 0
        enemy.physicsBody?.contactTestBitMask = bulletCategory
        enemy.physicsBody?.usesPreciseCollisionDetection = true
        
        let anim = SKAction.animateWithTextures([enemyTexture01, enemyTexture02], timePerFrame: 0.2)
        let fly = SKAction.repeatActionForever(anim)
        enemy.runAction(fly, withKey: "fly")
        self.addChild(enemy)
    }
    
    // MARK: contact
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        
        if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if (firstBody.categoryBitMask == enemyCategory) {
            if (secondBody.categoryBitMask == bulletCategory) {
                // 敵と弾が衝突したとき
                self.sparkOut(contact.contactPoint)
                // combo bounus
                var addScore = 10 + self.comboNum
                self.gage.addValue(1)
                if (gage.isGageFull()) {
                    specialShotButton.setMax(true)
                }
                
                score.addScore(addScore)
                comboNum = comboNum < comboManager.MAX_VALUE ? comboNum + 1 : comboNum
                self.comboManager.showComboNum(self, position: contact.contactPoint, value: comboNum)
                
                firstBody.node?.removeFromParent()
                secondBody.node?.removeFromParent()
                self.enemyCrashCount++
                self.totalEnemyCrashCount++
                self.updateDifficulty()
            }
        }
    }
    
    override func didSimulatePhysics() {
        self.enumerateChildNodesWithName("enemy") {
            node, stop in
            // 敵が画面外に出たら削除
            if (node.position.x < -25) {
                node.removeFromParent()
                self.life.addValue(-1)
                // game over
                if (self.life.getValue() <= 0 && self.gameState != GAME_STATE.GAME_OVER) {
                    self.view?.paused = true
                    self.gameState = GAME_STATE.GAME_OVER
                    NSUserDefaults.standardUserDefaults().setInteger(self.score.getScore(), forKey: "score")
                    self.delegate_escape!.sceneEscape(self)
                }
                self.flashBgColorOnce()
                if (self.life.getValue() == 1) {
                    self.flashBgColorForever()
                }
            }
            if (self.gameState == GAME_STATE.SPECAIL_SHOT_END) {
                self.player.setPlayerState(PLAYER_STATE.NORMAL)
                self.sparkOut(node.position)
                node.removeFromParent()
                
                var addScore = 10 + self.comboNum
                //self.gage.addValue(addScore)
                if (self.gage.isGageFull()) {
                    self.specialShotButton.setMax(true)
                }
                
                self.score.addScore(addScore)
                self.comboNum = self.comboNum < self.comboManager.MAX_VALUE ? self.comboNum + 1 : self.comboNum
                self.comboManager.showComboNum(self, position: node.position, value: self.comboNum)
                self.enemyCrashCount++
                self.totalEnemyCrashCount++
                self.updateDifficulty()
            }
        }
        self.enumerateChildNodesWithName("bullet") {
            // 弾が画面外に出たら削除
            node, stop in
            if (node.position.x > self.frame.width + 50 || node.position.y > self.frame.height + 50) {
                node.removeFromParent()
                // combo clear
                self.comboNum = 0
            }
        }
        // SpecialShotが終わった場合はNORMALモードに戻す
        if (self.gameState == GAME_STATE.SPECAIL_SHOT_END) {
            self.gameState = GAME_STATE.NORMAL
        }
    }
    
    // MARK: Effect
    // ----- ゲームスタート時のINTROテキストエフェクト 起動して最初の1回のみ表示される-----
    func showIntroText() {
        var centerPos = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        var texts: [SKLabelNode] = []
        for (var i = 0, n = 4 ; i < n ; i++) {
            var text = SKLabelNode(fontNamed: "MarkerFelt-Wide")
            //text.fontColor = UIColor.blackColor()
            text.fontColor = UIColor.whiteColor()
            text.fontSize = 15
            if (i % 2 == 0) {
                text.position = CGPoint(x: centerPos.x, y: centerPos.y + 90)
            } else {
                text.position = CGPoint(x: centerPos.x, y: centerPos.y + 40)
            }
            texts.append(text)
        }
        texts[0].text = "右側から敵が迫っている..！！"
        texts[1].text = "左側に到達する前に撃ち落とせ！"
        texts[2].text = "できる限り時間を稼いでくれ..."
        texts[3].text = "頼んだぞ.....！"
        
        var textFrame = SKShapeNode()
        var rect = CGRect(origin: CGPoint(x: centerPos.x - 130, y: centerPos.y), size: CGSize(width: 260, height: 140))
        textFrame.path = CGPathCreateWithRoundedRect(rect, 10.0, 10.0, nil)
        //textFrame.fillColor = UIColor(red: 0.93, green: 0.87, blue: 0.51, alpha: 1.0)
        textFrame.fillColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
        //textFrame.strokeColor = UIColor(red: 0.72, green: 0.53, blue: 0.04, alpha: 1.0)
        textFrame.strokeColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
        textFrame.zPosition = 200
        self.addChild(textFrame)

        textFrame.runAction(SKAction.fadeInWithDuration(0.3), completion: {
            // text表示
            let scaleUp = SKAction.scaleTo(1.1, duration: 0.1)
            let scaleDown = SKAction.scaleTo(1.0, duration: 0.1)
            let wait = SKAction.scaleTo(1.0, duration: 2.0)
            let fadeOut = SKAction.fadeOutWithDuration(0.2)
            let seq = SKAction.sequence([scaleUp, scaleDown, wait, fadeOut, SKAction.removeFromParent()])
            textFrame.addChild(texts[0])
            textFrame.addChild(texts[1])
            texts[0].runAction(seq)
            texts[1].runAction(seq, completion: {
                textFrame.addChild(texts[2])
                textFrame.addChild(texts[3])
                texts[2].runAction(seq)
                texts[3].runAction(seq, completion: {
                    textFrame.runAction(SKAction.sequence([fadeOut, SKAction.waitForDuration(0.3),  SKAction.removeFromParent()]), completion: {
                        didDiplyaedIntroFlg = true
                        self.showStartText()
                    })
                })
            })
        })
    }
    // ----- ゲームスタート時のテキストエフェクト showIntroText()の直後に呼ばれる -----
    func showStartText() {
        var readyText = SKLabelNode(fontNamed: "MarkerFelt-Wide")
        var goText = SKLabelNode(fontNamed: "MarkerFelt-Wide")
        
        readyText.text = "Ready....."
        goText.text = "Go"
        
        readyText.fontColor = UIColor.blackColor()
        readyText.fontSize = 30
        readyText.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMaxY(self.frame) - 200)
        
        goText.fontColor = UIColor.blackColor()
        goText.fontSize = 30
        goText.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMaxY(self.frame) - 200)
        
        let scaleUp = SKAction.scaleTo(2.0, duration: 0.1)
        let scaleDown = SKAction.scaleTo(1.5, duration: 0.1)
        let scaleStay = SKAction.scaleBy(1.0, duration: 1.0)
        let fadeout = SKAction.fadeOutWithDuration(0.1)
        let remove = SKAction.removeFromParent()
        let readyTextSequence = SKAction.sequence([scaleStay, fadeout, remove])
        let goTextSequence    = SKAction.sequence([scaleUp, scaleDown, scaleStay, fadeout, remove])
        
        self.addChild(readyText)
        readyText.runAction(readyTextSequence, completion: {
            self.addChild(goText)
            goText.runAction(goTextSequence, completion: {
                self.gameState = GAME_STATE.NORMAL
            })
        })
    }
    
    // ----- 敵を倒したときの演出エフェクト -----
    func sparkOut(location: CGPoint) {
        let path = NSBundle.mainBundle().pathForResource("spark", ofType: "sks")
        var particle = NSKeyedUnarchiver.unarchiveObjectWithFile(path!) as SKEmitterNode
        particle.name = "spark"
        particle.numParticlesToEmit = 100
        particle.position = location
        
        self.addChild(particle)
        let fadeOut = SKAction.fadeOutWithDuration(1.0)
        let remove = SKAction.removeFromParent()
        let sequence = SKAction.sequence([fadeOut, remove])
        particle.runAction(sequence)
    }
    
    // ----- 必殺ショットのテキストエフェクト. specialShot()の直前に実行される -----
    func showSpecialText() {
        self.gameState = GAME_STATE.SPECAIL_SHOT_BEGIN
        self.enumerateChildNodesWithName("enemy") {
            node, stop in
            node.physicsBody?.velocity.dx = 0
            node.removeActionForKey("fly")
        }
        var specialText = SKLabelNode(fontNamed: "MarkerFelt-Wide")
        specialText.text = "Come On!!"
        specialText.fontColor = UIColor.blackColor()
        specialText.fontSize = 20
        specialText.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        var rotate = SKAction.rotateByAngle(CGFloat(M_2_PI * 10), duration: 0.2)
        var scaleUp = SKAction.scaleTo(3.0, duration: 0.1)
        var scaleUpAndRotate = SKAction.group([rotate, scaleUp])
        var scaleDown = SKAction.scaleTo(2.5, duration: 0.1)
        var scaleStay = SKAction.scaleTo(2.5, duration: 0.2)
        var textFadeout = SKAction.fadeOutWithDuration(0.1)
        let textRemove = SKAction.removeFromParent()
        let textSequence = SKAction.sequence([scaleUpAndRotate, scaleDown, scaleStay, textFadeout, textRemove])
        self.addChild(specialText)
        specialText.runAction(textSequence, completion: {
            self.specialShot(self.player.getPlayerPos())
        })
    }
    
    // ----- テキストエフェクトに続く必殺ショットエフェクト -----
    func specialShot(location: CGPoint) {
        let path = NSBundle.mainBundle().pathForResource("specialShot", ofType: "sks")
        var specialShot = NSKeyedUnarchiver.unarchiveObjectWithFile(path!) as SKEmitterNode
        specialShot.name = "specialShot"
        specialShot.numParticlesToEmit = 200
        specialShot.position = location
        
        self.addChild(specialShot)
        let fadeOut = SKAction.fadeOutWithDuration(0.5)
        let remove = SKAction.removeFromParent()
        let sequence = SKAction.sequence([fadeOut, remove])
        specialShot.runAction(sequence, completion: {
            self.gameState = GAME_STATE.SPECAIL_SHOT_END
        })
    }
    
    // ----- ライフが1減ったときの演出用エフェクト -----
    func flashBgColorOnce() {
        let tmpRect = CGRectMake(0, 0, self.frame.width, self.frame.height)
        let path = CGPathCreateWithRect(tmpRect, nil)
        var bgRect = SKShapeNode()
        bgRect.path = path
        bgRect.fillColor = UIColor(red: 1.0, green: 0.27, blue: 0.0, alpha: 0.1)
        //bgRect.fillColor = UIColor(red: 1.0, green: 1.0, blue: 0.0, alpha: 0.1)
        bgRect.strokeColor = UIColor.clearColor()
        self.addChild(bgRect)
        let addAlpha = SKAction.fadeAlphaBy(8.0, duration: 0.1)
        let reduceAlpha = SKAction.fadeAlphaTo(0.0, duration: 0.1)
        let remove = SKAction.removeFromParent()
        let sequence = SKAction.sequence([addAlpha, reduceAlpha, remove])
        bgRect.runAction(sequence)
    }
    // ----- ライフが残り1のときの演出用エフェクト -----
    func flashBgColorForever() {
        let tmpRect = CGRectMake(0, 0, self.frame.width, self.frame.height)
        let path = CGPathCreateWithRect(tmpRect, nil)
        var bgRect = SKShapeNode()
        bgRect.path = path
        bgRect.fillColor = UIColor(red: 1.0, green: 0.27, blue: 0.0, alpha: 0.1)
        //bgRect.fillColor = UIColor(red: 1.0, green: 1.0, blue: 0.0, alpha: 0.1)
        bgRect.strokeColor = UIColor.clearColor()
        self.addChild(bgRect)
        let addAlpha = SKAction.fadeAlphaBy(5.0, duration: 0.3)
        let reduceAlpha = SKAction.fadeAlphaTo(0.0, duration: 0.5)
        let waitAction = SKAction.waitForDuration(1.0)
        let sequence = SKAction.sequence([addAlpha, reduceAlpha, waitAction])
        let action = SKAction.repeatActionForever(sequence)
        bgRect.runAction(action)
    }
    
    
    func sub(to: CGPoint, from: CGPoint) -> CGPoint {
        return CGPoint(x: to.x - from.x, y: to.y - from.y)
    }
    func length(v: CGPoint) -> CGFloat {
        return sqrt(v.x * v.x + v.y * v.y)
    }
    
    // ----- 敵が出現する5ラインを決定する(最初に1度didMoveToViewで呼ばれる) ------
    func dicideEnemyPos() {
        let centerEdge: CGPoint = CGPoint(x: UIScreen.mainScreen().applicationFrame.width, y: UIScreen.mainScreen().applicationFrame.height / 2)
        var enemyStartPos1 = CGPoint(x: centerEdge.x, y: centerEdge.y - 60)
        var enemyStartPos2 = CGPoint(x: centerEdge.x, y: centerEdge.y)
        var enemyStartPos3 = CGPoint(x: centerEdge.x, y: centerEdge.y + 60)
        var enemyStartPos4 = CGPoint(x: centerEdge.x, y: centerEdge.y + 120)
        var enemyStartPos5 = CGPoint(x: centerEdge.x, y: centerEdge.y + 180)
        self.enemyStattPositions.append(enemyStartPos1)
        self.enemyStattPositions.append(enemyStartPos2)
        self.enemyStattPositions.append(enemyStartPos3)
        self.enemyStattPositions.append(enemyStartPos4)
        self.enemyStattPositions.append(enemyStartPos5)
    }
    
    // ----- 難易度調整用function
    func updateDifficulty() {
        if (self.enemyCrashCount >= self.enemyCrashNumUnit) {
            // 難易度UP
            var descInterval = 0
            if (self.enemySpeed > -150) {
                descInterval = -25
                self.enemySpeed -= 18
            } else if (self.enemySpeed > -200) {
                self.enemyCrashNumUnit = 3
                descInterval = -15
                self.enemySpeed -= 15
            } else if (self.enemySpeed > -250) {
                self.enemyCrashNumUnit = 5
                descInterval = -3
                self.enemySpeed -= 3
            } else if (self.enemySpeed > -260) {
                self.enemySpeed = self.enemySpeed - 2 < self.ENEMY_SPEED_MAX ? self.ENEMY_SPEED_MAX : self.enemySpeed - 2
            } else {
                self.enemySpeed = self.enemySpeed - 1 < self.ENEMY_SPEED_MAX ? self.ENEMY_SPEED_MAX : self.enemySpeed - 1
            }
            var intervalMin = 0
            if (self.totalEnemyCrashCount > self.ENEMY_CRASH_LEVEL3) {
                intervalMin = self.ENEMY_INTERVAL_MIN_3
            } else if (self.totalEnemyCrashCount > self.ENEMY_CRASH_LEVEL2) {
                intervalMin = self.ENEMY_INTERVAL_MIN_2
            } else {
                intervalMin = self.ENEMY_INTERVAL_MIN_1
            }
            self.enemyAddInterval = self.enemyAddInterval + descInterval < intervalMin ? intervalMin : self.enemyAddInterval + descInterval
            // resert interval timer
            self.timer = 0
            self.enemyCrashCount = 0
            println("speed up!! \(self.enemySpeed)")
            println("interval up!! \(self.enemyAddInterval)")
            println("totalCrashNum : \(self.totalEnemyCrashCount)")
        }
    }
    /*
    func getHermiteCurvePointList(startPoint : CGPoint, vector0 : CGPoint, goalPoint : CGPoint, vector1 : CGPoint) -> Array<CGPoint> {
    var pointArray : [CGPoint] = []
    for (var i = 0, n = self.jumpTimeFrames; i < n; i++) {
    let u1 : CGFloat = CGFloat(i) * 1.0 / CGFloat(self.jumpTimeFrames - 1)
    let u2 : CGFloat = u1 * u1
    let u3 : CGFloat = u1 * u1 * u1
    let mP0 : CGFloat = 2 * u3 - 3 * u2 + 0 + 1
    let mV0 : CGFloat = u3 - 2 * u2 + u1
    let mP1 : CGFloat = -2 * u3 + 3 * u2
    let mV1 : CGFloat = u3 - u2
    let point = CGPointMake(startPoint.x * mP0 + vector0.x * mV0 + goalPoint.x * mP1 + vector1.x * mV1, startPoint.y * mP0 + vector0.y * mV0 + goalPoint.y * mP1 + vector1.y * mV1)
    pointArray.append(point)
    }
    return pointArray
    }
    */
}
