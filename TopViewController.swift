//
//  TopViewController.swift
//  ShootingSample
//
//  Created by Nobuhiro on 2015/01/11.
//  Copyright (c) 2015年 Nobuhiro Onishi. All rights reserved.
//

import UIKit
import GameKit

class TopViewController: UIViewController, GKGameCenterControllerDelegate, CommonAlertDelegate {
    var gameVc: GameViewController?
    var bannerViewFooter: GADBannerView?
    let request = GADRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBarHidden = true        
        self.view.backgroundColor = baseColor
        // フッター広告
        //bannerViewFooter = GADBannerView(adSize: GADAdSizeFullWidthPortraitWithHeight(kGADAdSizeBanner.size.height), origin: CGPointMake(0, self.view.frame.size.height - kGADAdSizeBanner.size.height))
        var offsetX = (self.view.frame.size.width - kGADAdSizeBanner.size.width) / 2
        bannerViewFooter = GADBannerView(adSize: kGADAdSizeBanner, origin: CGPointMake(offsetX, self.view.frame.size.height - kGADAdSizeBanner.size.height))
        bannerViewFooter?.adUnitID = "ca-app-pub-8756306138420194/4903153661" // 広告ユニットID
        bannerViewFooter?.rootViewController = self
        self.view.addSubview(bannerViewFooter!)
        bannerViewFooter?.loadRequest(request)
        
        
        self.loginGameCenter()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func tutorialButtonPushed(sender: AnyObject) {
        let tutorialViewController = self.storyboard?.instantiateViewControllerWithIdentifier("tutorialView") as TutorialViewController
        self.presentViewController(tutorialViewController, animated: true, completion: nil)
    }
    
    @IBAction func startButtonPushed(sender: AnyObject) {
        // in order not to make multi GameViewController instances
        if (gameVc == nil) {
            gameVc = self.storyboard?.instantiateViewControllerWithIdentifier("gameView") as? GameViewController
        }
        gameVc?.skView?.scene?.removeAllChildren()
        self.navigationController?.pushViewController(gameVc!, animated: true)
    }
    
    // notice : this is "ranking" button
    @IBAction func menuBUttonPushed(sender: AnyObject) {
        var localPlayer = GKLocalPlayer()
        
        localPlayer.loadDefaultLeaderboardIdentifierWithCompletionHandler({ (leaderboardIdentifier : String!, error : NSError!) -> Void in
            if error != nil {
                CommonAlert(delegate: self).showAlert(self, message: "ランキング機能を使用するにはGameCenterにサインインしてください")
                println(error.localizedDescription)
            } else {
                let gameCenterController:GKGameCenterViewController = GKGameCenterViewController()
                gameCenterController.gameCenterDelegate = self
                gameCenterController.viewState = GKGameCenterViewControllerState.Leaderboards
                gameCenterController.leaderboardIdentifier = "Eiden01" //該当するLeaderboardのIDを指定します
                self.presentViewController(gameCenterController, animated: true, completion: nil)
            }
        })
    }
    
    func loginGameCenter() {
        //GameCenterにログインします。
        let localPlayer = GKLocalPlayer()
        localPlayer.authenticateHandler = {(viewController, error) -> Void in
            if ((viewController) != nil) {
                println("ログイン確認処理：失敗-ログイン画面を表示")
                self.presentViewController(viewController, animated: true, completion: nil)
            }else{
                println("ログイン確認処理：成功")
                println(error)
                if (error == nil){
                    println("ログイン認証：成功")
                }else{
                    println("ログイン認証：失敗")
                }
            }
        }
    }
    
    // Delegate method for GKGameCenterDelegate
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController!){
        gameCenterViewController.dismissViewControllerAnimated(true, completion: nil);
    }
    
    // CommonAlertDelegate
    func buttonTapped(tag: Int) {
        switch tag {
        case 0 :
            self.dismissViewControllerAnimated(true, completion: nil)
            break
        default:
            break
        }
    }
    
    // UIAlertViewDelegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        switch buttonIndex {
        case 0 :
            switch alertView.tag {
            case 0 :
                self.dismissViewControllerAnimated(true, completion: nil)
                break
            case 1 :
                break
            default :
                break
            }
            break
        default:
            break
        }
    }
}
